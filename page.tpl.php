<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>
<a id="top"></a>
<div id="header">
	<div id="logo">
        <?php if ($site_name): ?>
		<h1><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>">
              <?php print $site_name; ?>
            </a>
        </h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
		<h2><?php print $site_slogan; ?></h2>
        <?php endif; ?>
	</div>
    <?php if ($primary_links): ?>
	<div id="menu">
    <?php print theme('menu_links', $primary_links); ?>
	</div>
    <?php endif; ?>
</div>
<hr />
<div id="page">
	<div id="bg">
		<div id="content">
        <?php print $breadcrumb; ?>
        <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
        <?php print $help; ?>
        <?php print $messages; ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
        
		</div>
        
        
       
		<div id="sidebar">
		  <?php if ($mission): ?><div id="mission"><h2>About:</h2><?php print $mission; ?></div><?php endif; ?>
          <?php if ($sidebar_right): ?>
		  <?php print $sidebar_right; ?>
			

		</div>
        <?php endif; ?>
		<!-- end sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
</div>
<!-- end page -->
<hr />
<div id="footer">
<?php print $footer_message; ?>
	<p>Design by <a href="http://www.nodethirtythree.com/">NodeThirtyThree</a> | Themed by <a href="http://anthonylicari.com" title="Anthony Licari">Licari</p>
    <?php print $closure; ?>
</div>


